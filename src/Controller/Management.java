package Controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author anhnb
 */
public class Management {

    public Map<String, Integer> countWord(String s) {
        Map<String, Integer> map = new HashMap();
        StringTokenizer str = new StringTokenizer(s);
        String key;
        while (str.hasMoreTokens()) {
            key = str.nextToken();
            if (map.containsKey(key)) {
                map.replace(key, map.get(key) + 1);
            } else {
                map.put(key, 1);
            }
        }
        return map;
    }

    public Map<Character, Integer> countCharacter(String s) {
        Map<Character, Integer> map = new HashMap();
        char key;
        for (int i = 0; i < s.length(); i++) {
            key = s.charAt(i);
            if(Character.isSpaceChar(key)) continue;
            if (map.containsKey(key)) {
                map.replace(key, map.get(key) + 1);
            } else {
                map.put(key, 1);
            }
        }
        return map;
    }

    public void menu() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter your content: ");
        String s = in.nextLine();
        System.out.println(countWord(s));
        System.out.println(countCharacter(s));
    }

}
